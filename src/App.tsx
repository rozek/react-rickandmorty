import React from 'react'; 

import './App.css'

import Form from './components/form/Form';
import PersonList from './components/personList/PersonList';
import Payments from './components/Payments/Payments';

// Interface needed to setState
interface IState {
    characters?: [];
}
class App extends React.Component <IState>{
    state: IState = {
        characters: []
    }

    getCharacter = (e:any, charak:object):object => {
        const characterName = e.target.elements.characterName;
        e.preventDefault();
        console.log('eloooo!!!!!!!!!!');
        charak= {characterName:e.target.elements.characterName.value}
        console.log(charak);
        return charak;   
    } 
    componentDidMount() {
        // console.log(this.getCharacter.charakRet);  
        var stan =this.setState({characters: 'test'});
        console.log(stan);
        
        // console.log(this.getCharacter);
        
    }
    render() {
        return(     
            <div className="App">
                <header className="App-header">
                    <h1 className="App-title"> Rick and Morty Characters</h1>
                </header>
                <h2>{this.state.characters}Hello</h2>
                <Form  getCharacter={this.getCharacter}/>
                <PersonList/>
                <Payments/>
            </div>
        )
    }
}

export default App;