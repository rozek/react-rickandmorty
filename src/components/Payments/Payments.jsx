import React, { useState, useRef, useEffect } from 'react';

// declare var window: Window;

function Payments() {

    

    const [paidFor, setPaidFor] = useState(false);
    const [loaded, setLoaded] = useState(false);

    let paypalRef = useRef();
  

    const product = {
        price: 10.02,
        description: "fancy picture, get one"
    };

    // interface Window {
    //     paypal?: any;
    // }
    
  
    
    // if (window.paypal !== undefined) {
    //     console.log(window.paypal);
    // }

    useEffect(() => {

        
        //Load PayPal Script
        const script = document.createElement('script');
        script.src = 
        "https://www.paypal.com/sdk/js?client-id=AQcPp3rVC4Ci_mUDsOcnSj5IglexgKvrbAtD6KKXjwhppgfcuZivWjnixRN1gb7NHeYCYfXnteTvAHV7";
        script.addEventListener("load", () => setLoaded(true));
        document.body.appendChild(script);

        if (loaded) {
            setTimeout(() => {
                window.paypal
                .Buttons({
                    createOrder: (data,actions) => {
                        return actions.order.create({
                            purchase_units: [
                                {
                                    description: product.description,
                                    amount: {
                                        currency_code: "USD",
                                        value: product.price
                                    }
                                }
                            ]
                        });
                    },
                    onApprove: async (data, actions) => {
                        const order = await actions.order.capture();

                        setPaidFor(true)
                        
                        console.log(order);
                        
                    },
                })
                .render(paypalRef)
            });
        }
    })

        return (
            <div className="money">
               {paidFor ? (
                   <div>
                     <h1>Congrats, now You own this picture</h1>   
                     {/* <img src={gif}/> */}
                   </div>
               ): (
                <div>
                <h1>{product.description} for ${product.price}</h1>   
                {/* <img src={chair} width="200"/> */}
                <div ref={v => (paypalRef = v)}></div>
              </div>
               )} 
            </div>
        )
    }

export default Payments;