import React from 'react';

import axios from 'axios';

import './personList.css'

export default class PersonList extends React.Component {
  state = {
    persons: []
  }

  componentDidMount() {
   const getData =  axios.get(`https://rickandmortyapi.com/api/character/`)
      .then(res => {
        const persons = res.data.results;
        this.setState({ persons });
        console.log(persons);
      }) 
  }

  render() {
    return (
      <div className='cardBox'>
        { 
        this.state.persons.map(person => 
        <div className='cards' key={person.id}><img src={person.image} alt={person.name}></img>
        <div className="name">{person.name}</div>
        </div>
        )
        }
      </div>
    )
  }
}

/* 
https://rickandmortyapi.com/api/character/
 */
